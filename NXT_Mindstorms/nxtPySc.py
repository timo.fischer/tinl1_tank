#!/usr/bin/env python
#

import nxt
import nxt.locator
from nxt.sensor import *
from nxt.motor import *
import paho.mqtt.subscribe as subscribe

def mqtt():
    msg = subscribe.simple("tank/aktor/bewegung", hostname="localhost")
    return str(msg.payload.decode("utf-8"))

ch = ' '

with nxt.locator.find() as b:
    left = b.get_motor(nxt.motor.Port.A)
    right = b.get_motor(nxt.motor.Port.B)
    both = nxt.motor.SynchronizedMotors(left, right, 0)
    leftboth = nxt.motor.SynchronizedMotors(left, right, 100)
    rightboth = nxt.motor.SynchronizedMotors(right, left, 100)

    while ch != 'quit':
        ch = mqtt()
        print(ch)
        if ch == 'up':
            both.turn(80, 360, True)
        elif ch == 'down':
            both.turn(-80, 360, True)
        elif ch == 'left':
            leftboth.turn(80, 90, True)
        elif ch == 'right':
            rightboth.turn(80, 90, True)
        elif ch == 'shoot':
            both.turn(80, 10, True)
            b.play_tone(50,100)
