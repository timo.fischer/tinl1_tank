from sense_hat import SenseHat
import sys

sense = SenseHat()

R = [255, 0, 0]  # Red
W = [255, 255, 255]  # White
B = [0, 0, 0]  # Blank

X1 = B
X2 = B
X3 = B
X4 = B
X5 = B
X6 = B
X7 = B
X8 = B
X9 = B
X10 = B
X11 = B
X12 = B
Y1 = B
Y2 = B
Y3 = B
Y4 = B
Y5 = B
Y6 = B
Y7 = B
Y8 = B
Y9 = B
Y10 = B
Y11 = B
Y12 = B

O = B
X = W

input = sys.argv[1]

if input == '1' or input == '1s':
    X1 = W
    Y1 = W
    if 's' in input:
        Y1 = R
elif input == '2' or input == '2s':
    X2 = W
    Y2 = W
    if 's' in input:
        Y2 = R
elif input == '3' or input == '3s':
    X3 = W
    Y3 = W
    if 's' in input:
        Y3 = R
elif input == '4' or input == '4s':
    X4 = W
    Y4 = W
    if 's' in input:
        Y4 = R
elif input == '5' or input == '5s':
    X5 = W
    Y5 = W
    if 's' in input:
        Y5 = R
elif input == '6' or input == '6s':
    X6 = W
    Y6 = W
    if 's' in input:
        Y6 = R
elif input == '7' or input == '7s':
    X7 = W
    Y7 = W
    if 's' in input:
        Y7 = R
elif input == '8' or input == '8s':
    X8 = W
    Y8 = W
    if 's' in input:
        Y8 = R
elif input == '9' or input == '9s':
    X9 = W
    Y9 = W
    if 's' in input:
        Y9 = R
elif input == '10' or input == '10s':
    X10 = W
    Y10 = W
    if 's' in input:
        Y10 = R
elif input == '11' or input == '11s':
    X11 = W
    Y11 = W
    if 's' in input:
        Y11 = R
elif input == '12' or input == '12s':
    X12 = W
    Y12 = W
    if 's' in input:
        Y12 = R


rotation = [
Y11,O ,O ,Y12,Y1,O,O,Y2,
O,X11,O,X12,X1,O,X2,O,
O,O,X11,X12,X1,X2,O,O,
Y10,X10,X10,X,X,X3,X3,Y3,
Y9,X9,X9,X,X,X4,X4,Y4,
O,O,X8,X7,X6,X5,O,O,
O,X8,O,X7,X6,O,X5,O,
Y8,O,O,Y7,Y6,O,O,Y5
]

sense.set_pixels(rotation)