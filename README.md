# TinL1 Projekt Ferngesteuerter Panzer

## Projektübersicht
![ALT](/Bilder/Systemabbild.png)

### Nötige Hardware
Die Folgende Hardware wird für das Projekt verwendet:
- Raspberry Pi 4B samt mikro-SD-Karte (32 GB)
- Netzteil USB-C (5,1 V / 3 A)
- Raspberry Pi Kamera (egal welche Version) mit entsprechendem Flachbandkabel
- Sense hat Version 1 Extension Board für den Raspberry Pi
- Mindstorms-NXT Block mit 6 mal AA-Batterie
- Zwei Mindstorms-NXT Motoren samt Kabel
 
### Projekt-Output
Mit dem Dashboard in Home Assitant, ist es möglich ein Fahrzeug (Panzer) zu steuern und dessen Umgebungsvariablen zeitgleich einzusehen. 
![ALT](/Bilder/Panzer_Komplett.jpg)

## Installation Raspberry Pi OS
Als erster Schritt, soll das neuste Raspberry Pi OS (64 Bit) Image auf eine Formatierte Micro-SD Karte geschrieben werden.
Bei den erweiterten Einstellungen müssen folgende Werte angegeben werden. (in meinem Fall)
```
Hostname: tank
SSH aktivieren
	Passwort zur Authentifizierung verwenden
Benutzername und Passwort setzten:
	Benutzername: rptf
	Passwort: ******
Wifi einrichten
	SSID: ISE-Lab-N0042
	Passwort: 31.19.40
	Wifi-Land: CH
Spracheinstellungen festlegen:
	Zeitzone: Europe/Berlin
	Tastaturlayout: ch
```
Nach dem Schreibeprozess kann die SD-Karte entfernt und in den Raspberry Pi eingesetzt werden.
Sobald der Raspberry Pi das erste Mal gebootet hat, kann mittels SSH auf den Pi verbunden werden.

## Zeit Einstellungen
Kontrolliere mit folgendem Befehl das Datum und die Uhrzeit deines Raspberry Pi
```
date
```
Falls die Uhrzeit und das Datum korrekt sind, kannst du folgende Schritte dieses Kapitels überspringen. Ansonsten musst du weitere NTP-Fallback Server dem Raspberry Pi hinzufügen. «Dies kann der Fall sein, wenn du dich in einem Netzwerk befindest, welches NTP «Network Time Protokoll» anfragen nicht nach aussen weiterleitet. Hierbei verfügt das Netzwerk meist über einen eigenen NCP-Server»
Um weitere NTP-Server hinzuzufügen, öffne folgende Datei.
```
sudo nano /etc/systemd/timesyncd.conf
```
In der Datei musst du die Zeile 15 und 16 Anpassen. Dabei entfernst du die «Hash» Zeichen (#) vor den Zeilen:
```
#NTP=
#FallbackNTP=
```
Weiter fügst du hinter der Zeile 16 («FallbackNTP») deine weiteren NTP-Server Adressen hinzu.
Im Falle der FHNW: ntp1.fhnw.ch ntp2.fhnw.ch
<bk> Wichtig! Lösche dabei die bestehenden Adressen nicht!

Nach dem Speichern der Datei, musst du folgende Befehle eingeben, um die Einstellungen zu übernehmen.
```
timedatectl set-ntp true
```
```
sudo reboot
```

## Node-Red Installation
Als erstes muss die Nodejs vorgängig installiert werden. Dafür musst du folgende Befehle eingeben.
```
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs
```
Mit folgenden Befehlen kanns du nun die Installation überprüfen
```
node --version
```
```
npm --version
```
Die Version von node sollte höher wie 18.** sein und die Version von npm höher wie 8.**

Nun kannst du Node-Red mit folgendem Befehl installieren.
```
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```
Nach der Installation wirst du gefragt, ob du die Einstellungen von Node-Red machen möchtest antworte Hierbei mit «y». Danach wirst du Schritt weise über alle Konfigurationspunkte abgefragt.

Du kannst auf die Einstellungen jedoch auch über folgende Datei zugreifen und bearbeiten.
```
sudo nano ~/.node-red/settings.js
```
Nimm die folgenden Einstellungen vor:
![ALT](/Bilder/NodeRedConfig.png)
Nun kannst du Node-Red Starten, um die Funktion zu überprüfen
```
node-red-start
```
Du kannst dich über den Link: [IP-Adresse-Raspberry Pi]:1880 über ein Browers einloggen.

Wenn alles Funktioniert kannst du Node-Red als Systemctl-Service automatisch starten lassen.
```
sudo systemctl enable nodered.service
```
```
sudo systemctl start nodered.service
```

### Installation der Node Addons
Die folgenden Addons musst du auf Node-Red über den Webbrowser installieren. Öffne Hierfür den Webbrowser und verbinde dich mit dem Node-Red Webserver: [IP-Adresse-Raspberry Pi]:1880

Gehe auf «Palette verwalten» anschliessend auf den Reiter «Installation» Im Fenster «Nodes Filtern» kannst du nun die folgenden Suchbegriffe eingeben und Installieren.
- node-red-node-pi-sense-hat
- node-red-contrib-influxdb
- node-red-contrib-pythonshell

Nach der Installation muss noch eine Anpassung im Python Treiber des Senshats gemacht werden. Öffne hierfür die folgende Datei
```
sudo nano /usr/lib/python3/dist-packages/sense_hat/sense_hat.py
```
Anschliessend suche mit der Tastenkombination (Ctrl+W) nach «TCS». Kommentiere den ganzen Code-Abschnitt mit # aus
```
        # initialise the TCS34725 colour sensor (if possible)
        #try:
        #    self._colour = ColourSensor()
        #except Exception as e:
        #   logging.warning(e)
        #   pass
```
Nach dem Speichern der Datei starte Node-Red neu.
```
node-red-restart
```

## Docker Installation
Als erstes wird ein neues Docker-Repository angelegt. Hierfür musst du zuerst sicherstellen, dass dein Betriebssystem auf dem neusten Stand ist. Gib dafür folgende Befehle eingeben.
```
sudo apt-get update
```
```
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```
Anschliessend muss der Offizielle GPG-Schlüssel von Docker im Betriebssystem hinterlegt/gespeichert werden. Dafür gibst du folgenden Befehl ein.
```
sudo mkdir -p /etc/apt/keyrings
```
```
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
Nun kannst du das Docker Repository auf deinem Raspberry Pi Klonen mit folgendem Befehl.
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
Anschliessend wird die Docker-Engine auf dem Raspberry Pi installiert mit:
```
sudo apt-get update
```
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
Um den Umgang später mit den Docker-Containern zu vereinfachen wird nun eine Docker-groupe angelegt und mit dem aktiven Benutzer verknüpft. So können später Docker-Befehle ohne Adminrechte ausgeführt werden.
```
sudo groupadd docker
```
```
sudo usermod -aG docker $USER
```
```
newgrp docker
```
Überprüfen, ob die Installation samt der Benutzerverknüpfung funktioniert hat, kann man mit folgendem Befehl. Hierbei sollte das Docker Image «hello-world» heruntergeladen und als Ausgabe angezeigt werden.
```
docker run hello-world
```
Zusätzlich kannst du die Version von Docker-Compose überprüfen.
```
docker compose version
```
Für die weiteren Installationen solltest du ein neues Verzeichnis in dem Home Verzeichnis erstellen, in welchem alle Docker Compose Dateien gespeichert werden.
```
mkdir docker
```

## Influxdb Installation
Als erstes, solltest du im bereits erstellen Verzeichnis «docker» ein neues Verzeichnis erstellen.
```
mkdir docker/influxdb
```
Als nächstes erstellst du in diesem Verzeichnis eine neue Datei mit dem Namen 
«docker-compose.yml»
```
nano docker-compose.yml
```
Kopiere folgenden Inhalt in die Datei und speichere sie ab.
```
version: "3"
services:
  influxdb:
    image: influxdb:latest
    container_name: influxdb
    restart: always
    ports:
      - "8086:8086"
    environment:
      - INFLUXDB_MONITOR_STORE_ENABLED=false
      - INFLUXDB_REPORTING_DISABLED=true
    volumes:
      - ./influxdb/data:/var/lib/influxdb2
      - ./influxdb/backup:/var/lib/influxdb2/backup
```
Nach dem Speichern der Datei kannst du die Datei mit «Docker-Compose» als Docker Container starten.
```
docker compose up -d
```
Mit folgendem Befehl kannst du nun kontrollieren, ob der Container auch läuft. 
```
docker ps
```
Zur weiteren Überprüfung kannst du in deinem Webbrowser die Seite: 
[IP-Adresse-Raspberry Pi]:8086 aufrufen. Die Konfiguration der InfluxDB wird in einem späteren Zeitpunkt erläutert.

## Grafana Installation
Als erstes, solltest du im bereits erstellen Verzeichnis «docker» ein neues Verzeichnis erstellen.
```
mkdir docker/grafana
```
 Als nächstes erstellst du in diesem Verzeichnis eine neue Datei mit dem Namen 
«docker-compose.yml»
```
nano docker-compose.yml
```
Kopiere folgenden Inhalt in die Datei und speichere sie ab.
```
version: "3"  
services:
  grafana:
    image: grafana/grafana:latest
    container_name: grafana
    restart: always
    user: "1000"
    ports:
      - "3000:3000"
    environment:
      - GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_AUTH_BASIC_ENABLED=false
      - GF_ENABLE_GZIP=true
      - GF_USERS_DEFAULT_THEME=dark
      - GF_SECURITY_COOKIE_SAMESITE=disabled
      - GF_SECURITY_ALLOW_EMBEDDING=true
      - GF_DASHBOARDS_MIN_REFRESH_INTERVAL=1s
    volumes:
      - ./grafana/data:/var/lib/grafana
      - ./grafana/log:/var/log/grafana 
      - ./grafana/provisioning/datasources:/etc/grafana/provisioning
    extra_hosts:
      - host.docker.internal:host-gateway
```
Nach dem Speichern der Datei kannst du die Datei mit «Docker-Compose» als Docker Container starten.
```
docker compose up -d
```
Sobald der Container erstellt wurde, musst du noch eine Anpassung vornehmen. Die Zugriffsberechtigung des Verzeichnis «data» muss auf die Gruppe «472» eingestellt werden. Dafür verwendest du folgenden Befehl.
```
sudo chmod 472 ./grafana/data
```
Anschliessend kannst du den Container neustarten mit dem Befehl.
```
docker compose restart
```
Mit folgendem Befehl kannst du nun kontrollieren, ob der Container auch läuft. Falls unter der Spalte «PORTS» etwas angezeigt wird, hat alles funktioniert.
```
docker ps
```
Zur weiteren Überprüfung kannst du in deinem Webbrowser die Seite: 
[IP-Adresse-Raspberry Pi]:3000 aufrufen. Die Konfiguration von Grafana wird in einem späteren Zeitpunkt erläutert.

## MQTT Broker Installation
Als erstes, solltest du im bereits erstellen Verzeichnis «docker» ein neues Verzeichnis erstellen.
```
mkdir docker/mqtt
```
Als nächstes erstellst du in diesem Verzeichnis eine neue Datei mit dem Namen 
«docker-compose.yml»
```
nano docker-compose.yml
```
Kopiere folgenden Inhalt in die Datei und speichere sie ab.
```
services:
  mosquitto:
    image: eclipse-mosquitto
    container_name: mqtt_prjbroker
    restart: always
    volumes:
      - ./mosquitto/data:/mosquitto/data
      - ./mosquitto/log:/mosquitto/log
      - ./mosquitto/config:/mosquitto/config
    ports:
      - "1883:1883"
      - "9001:9001"
```
Nach dem Speichern der Datei kannst du die Datei mit «Docker-Compose» als Docker Container starten.
```
docker compose up -d
```
Sobald der Container erstellt wurde, musst du noch eine Anpassung vornehmen. Dafür erstellst du eine neue Datei im eben generierten Unterverzeichnis.
```
sudo nano mosquitto/config/mosquitto.conf
```
Mit folgendem Inhalt:
```
persistence true
persistence_location /mosquitto/data/
log_dest file /mosquitto/log/mosquitto.log
listener 1883
## Authentication ##
allow_anonymous true
```
Anschliessend kannst du den Container neustarten mit dem Befehl.
```
docker compose restart
```
Mit folgendem Befehl kannst du nun kontrollieren, ob der Container auch läuft.
```
docker ps
```
Falls unter der Spalte «PORTS» etwas angezeigt wird, hat alles funktioniert.

## Home Assistant Installation
Als erstes, solltest du im bereits erstellten Verzeichnis «docker» ein neues Verzeichnis erstellen.
```
mkdir docker/homeassistant
```
Als nächstes erstellst du in diesem Verzeichnis eine neue Datei mit dem Namen 
«docker-compose.yml»
```
nano docker-compose.yml
```
Kopiere folgenden Inhalt in die Datei und speichere sie ab.
```
version: '3'
services:
  homeassistant:
    container_name: homeassistant
    image: "ghcr.io/home-assistant/home-assistant:stable"
    volumes:
      - ./homeass/config:/config
      - /etc/localtime:/etc/localtime:ro
    restart: unless-stopped
    privileged: true
    network_mode: host
```
Nach dem Speichern der Datei kannst du die Datei mit «Docker-Compose» als Docker Container starten.
```
docker compose up -d
```
Mit folgendem Befehl kannst du nun kontrollieren, ob der Container auch läuft. 
Achtung! Unter der Spalte «PORTS» sollte nichts zu sehen sein.
```
docker ps
```
Zur weiteren Überprüfung kannst du in deinem Webbrowser die Seite: 
[IP-Adresse-Raspberry Pi]:8123 aufrufen. Die Konfiguration des Home Assistant wird in einem späteren Zeitpunkt erläutert.

## Konfigurationen
- Node-Red	[IP-Adresse-Raspberry Pi]:1880
- InfluxDB	[IP-Adresse-Raspberry Pi]:8086
- Grafana	[IP-Adresse-Raspberry Pi]:3000
- Home Assistant	[IP-Adresse-Raspberry Pi]:8123

### Pi-Kamera Konfiguration
Um auf die Raspberry Pi Kamera zu zugreifen, muss als erster Schritt der Legacy Treiber aktiviert werden, da es beim neuen Treiber zu Problemen kommen kann. Hierfür öffnest du die Raspi-config mit folgendem Befehl.
```
sudo raspi-config
```
Mit den Pfeil-Tasten musst du nun zum Punkt «3 Interface Options» navigieren und mit der Return-Taste bestätigen. Anschliessend wählst du den Punkt «I1 Legacy Camera» aus und aktivierst diesen. Darauf kommt eine Warnung, welche mit «OK» bestätigt werden soll. Nun kannst du mit der Pfeil-Taste nach Links «Finish» auswählen und führst anschliessend ein Reboot durch.

Wenn der Raspi erneut gebootet hat, kannst du nun mit der Installation von Motion beginnen. Dieses Programm wird später ein Live-Web stream der Raspi-Kamera zur Verfügung stellen.

Um den Raspberry Pi auf den neusten Stand zu bringen, führst du zuerst folgende Befehle aus.
```
sudo apt-get -y update
```
```
sudo apt-get -y upgrade
```
Nun kannst du Motion installieren mit folgendem Befehl.
```
sudo apt-get install motion
```
Sobald Motion installiert wurde, muss die Konfigurationsdatei angepasst werden. Hierfür kannst du folgenden Befehl eingeben.
```
sudo nano /etc/motion/motion.conf
```
Folgende Parameter müssen in der Konfiguration angepasst werden
```
-	deamon on
-	movie_output off
-	webcontrol_port 9090
-	webcontrol_localhost off
-	stream_port 9091
-	stream_localhost off
-	stream_maxrate 60 (falls dieser Parameter nicht angezeigt wird, muss er manuell hinzugefügt werden)
```
Nach dem Speichern der Datei muss nun noch eine weitere Datei manuell erstellt werden.
```
sudo nano /etc/default/motion
```
Diese soll den folgenden Inhalt enthalten.
```
start_motion_deamon=yes
```
Nun kanns du Motion erstmals Testen auf seine Funktion. Gib hierfür folgenden Befehl ein.
```
sudo motion
```
Zur Überprüfung kannst du in deinem Webbrowser die Seite: 
[IP-Adresse-Raspberry Pi]:9091 aufrufen. Es sollte das Live-Bild deiner Raspi-Kamera Abgebildet werden.

Es gibt jedoch noch ein Problem. Die Logdateien von Motion können nicht gespeichert werden. Um dies zu beheben, musst du die Datei-Berechtigungen angepasst werden.
```
sudo chmod 777 /var/log/motion/motion.log
```
Anschliessend muss der Motion-Service neugestartet werden.
```
sudo service motion restart
```

### Sensordaten übermitteln
#### _Node-Red_
Als erstes wird ein neuer Flow in Node-Red eingefügt. Dafür ist eine Datei im Repository «TinL1_tank/Node_Red/Sensordaten.json» abgelegt. Zugreifen auf das Repository kannst du über folgende URL
https://gitlab.fhnw.ch/timo.fischer/tinl1_tank.git

Die Datei kann mittels Drag&Drop oder über die Funktion «Import» im Webbrowser hochgeladen werden. Danach wird der Knopf **«Übernahme (deploy)»** betätigt.
	
#### _InfluxDB_
Nun öffnest du den Webserver von **InfluxDB**. Es sollte ein Willkommensbildschirm angezeigt werden.
drücke auf **Get Started** und gib einen Benutzernamen sowie Passwort ein. Als **Initial Organisation Name** musst du _«tank»_ eingeben und als **Initial Bucket Name** gibst du _«umgebung»_ ein. Anschliessend drückst du auf **Quick Start**.

Um die Späteren Daten auf der Datenbank ablegen zu können, solltest du nun die sogenannten Buckets bearbeiten.

Gehe hier für als erstes auf **Load Data**, Anschliessend in der Registerkarte auf **BUCKETS** und 
**+ CREATE BUCKET** dabei öffnet sich ein neues Fenster. Als Namen gibst du nun _«bewegung»_ ein und wählst aus, dass die Daten nach 30 Tagen gelöscht werden sollen. Den Bucket _«umgebung»_ kannst du hierbei auch bearbeiten und auf die 30 Tage einstellen.

Nun musst du ein Token generieren, welches für die sichere Datenübertragung zwischen Node-Red zu InfluxDB, sowie auch InfluxDB zu Grafana, nötig sein wird.

Gehe hier für als erstes auf **Load Data**, Anschliessend in der Registerkarte auf **API TOKEN** und 
**+ GENERATE API TOKEN** dabei wählst du **All Access API Token** aus. In einem Neuen Fester kannst du nun dem Token einen freien Namen Geben und auf **SAVE** klicken.

**Wichtig!** Nach dem du dies Gespeichert hast wird dir das Token auf dem Bildschirm als Zeichenfolge angezeigt. Dies ist der einzige Zeitpunkt für dich an welches das Token ersichtlich ist. Kopiere das Token also und speichere es an einem sicheren Ort ab.
	
#### _Node-Red_
In **Node-Red** bearbeitest du nun einer der beiden Blöcke _«localInflux»_ und drückst auf das 
Bearbeitungs-Symsbol. Füge nun unter dem Punkt «Token», das eben kopierte Token ein und speichere den Block **(Aktualisieren)**. Nun kannst du den Flow übernehmen, mit der Schaltfläche 
**Übernahme (deploy)**.

#### _InfluxDB_
Die Messdaten werden nun über Node-Red an die Influx Datenbank übermittelt. Du kannst dies überprüfen in dem du auf der InfluxDB die folgenden Filter in der entsprechenden Reihenfolge unter **Data Explorer** einstellst. 
Für die Bewegungs-Variablen
```
FROM: bewegung | Filter, device, tank | Filter, movement, (und dann die gewünschte Variabel)
```
Für die Umgebungs-Variablen
```
FROM: umgebung | Filter, device, tank | Filter, _field, (und dann die gewünschte Variabel)
```
 
### Sensordaten Darstellen in Grafana
#### _Grafana_
Als erstes öffnets du den Webserver von Grafana. Bei dem ersten Aufruf sollte ein Willkommensbildschirm angezeigt werden bei welchem du ein Loggin erstellen kannst. Falls dies nicht der Fall sein sollte und du auf der **«Home»** Seite landest, kannst du auf die Schaltfläche **«Sign in»** klicken.

Die Standart Loggindaten von Grafana sind _(Username: admin, Password: admin)_. Es ist von Vorteil das Passwort zu ändern!

Wenn du nun eingelogg bist, befindest du dich auf der **«Home»** Seite von Grafana. Nun musst du die Datenquelle hinzufügen. Dafür drückst du auf die Schaltfäche **«Configuration»** und anschliessend auf **«Data sources»** Danach auf **«Add data source»**.

Es erscheint eine Auswahl an verschiedenen Datenbanken. Wähle **«InfluxDB»** und ein Einstellungsfenster öffnet sich.
Stelle die folgenden Parameter ein:
```
Query Language: 	Flux
HTTP
	- URL: 		http://host.docker.internal:8086
Auth
	- Basic auth: 	True
Basic Auth Details
	- User: 		[Benutzername von InfluxDB]
	- Password: 	[Passwort von InfluxDB]
InfluxDB Details
	- Organization: 	tank
	- Token: 	[das gespeicherte Token der InfluxDB]
```
Anschliessend mit **«Save & test»** bestätigen. Dabei sollte eine Nachricht erscheinen mit einem grünen Hacken. _«datasource is working. 4 buckets found»_

Nun kannst du das Dashboard erstellen. Klicke hierfür auf die Schaltfläche **«Dashboard»** und anschliessend auf **«Import»**. Nun kannst du die Datei _«Tank-Dashboard.json»_ im Ordner **«Grafana»** aus dem GitLab-Repository hochladen. Unter dem Punkt **«InfluxDB»** musst du im Dropdown-Menü noch **«InfluxDB (default)»** auswählen und drückst dann auf **«Import»**.

Das Dashboard sollte nun auf deinem Bildschirm erscheinen und es sollten die Beschleunigungsdaten deines Raspberry Pi’s angezeigt werden.

### Einrichten von Home Assistant
#### _Home Assistant_
Als erstes öffnest du Home Assistant über den Webbrowser. Du solltest beim ersten Aufruf direkt einen Registrierungs Bildschirm Angezeigt bekommen. Registriere dich dabei mit einem Nutzernamen und Passwort. Aschliessend kannst du dem Setup folgen und die gewünschten angeben eingeben. Es sollen vorerst keine weiteren Plugins installiert werden. (Da dies kann zu Abstürken führen kann)

Nun fügen wir den MQTT-Dienst in Home Assistant dazu. 
hierfür gest du auf **«Einstellungen»**, **«Geräte & Dienste»** und **«+ INTEGRATION HINZUFÜGEN»**. Im neuen Fenster suchst  du nach _«MQTT»_ und klickst auf das Suchergebnis. Beim Punkt **«Server»** gibst du _«localhost»_ ein und drückst anschliiessend auf **«SAVE»**.

Nun kannst du die Verbindung kontrollieren, in dem du auf den neuen Dienst **«localhost MQTT»** (unter **«Integrationen»**) klickst **«KONFIGURIEREN»**. Im neuen Fesnter kannst du unter dem Punkt **«Auf ein Topic hören»** ein Hash-Zeichen _«#»_ einfügen und **«ANFANGEN ZUHÖREN»** anwäheln. Nun sollten die MQTT-Nachrichten unterhalb zyklisch erscheinen.

----
Um die Nachrichten aus dem MQTT-Broadcast als Entität lesen zu können, ist eine Konfiguration der Config-Datei von Home Assistant nötig. Gehe hierfür in folgendes Verzeichnis.
```
sudo nano ~/docker/homeassistant/homeass/config/configuration.yaml
```
Füge nun am Ende der Datei die folgenden Code-Zeile hinzu und speichere die Datei erneut ab.
```
# MQTT Entity definitions
mqtt: !include mqtt.yaml
```
Nun erstellst du eine neue Datei mit dem Namen «mqtt.yaml» und fügst den untenstehenden Inhalt ein.
```
sudo nano ~/docker/homeassistant/homeass/config/mqtt.yaml
```
```ruby
# MQTT Entity definitions
sensor:
  - name: "Temperatur"
    state_topic: "tank/sensor/umgebung"
    unit_of_measurement: "C°"
    value_template: "{{value_json.temperatur}}"
  - name: "Humidity"
    state_topic: "tank/sensor/umgebung"
    unit_of_measurement: "g/m^3"
    value_template: "{{value_json.humidity}}"
  - name: "X-Acceleration"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.acceleration.x}}"
  - name: "Y-Acceleration"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.acceleration.y}}"
  - name: "Z-Acceleration"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.acceleration.z}}"
  - name: "X-Gyroscope"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.gyroscope.x}}"
  - name: "Y-Gyroscope"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.gyroscope.y}}"
  - name: "Z-Gyroscope"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.gyroscope.z}}"
  - name: "Roll-Orientation"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.orientation.roll}}"
  - name: "Pitch-Orientation"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.orientation.pitch}}"
  - name: "Yaw-Orientation"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.orientation.yaw}}"
  - name: "Compass"
    state_topic: "tank/sensor/bewegung"
    value_template: "{{value_json.compass}}"
```
----
#### _Home Assistant_
Um die Konfigurationsdatei neu zu laden, Klickst du auf die **«Entwicklerwerkzeuge»** und anschliessend auf **«KONFIGURATION PRÜFEN»**. Wenn ein grüner Text erscheint kannst du anschliessend auf **«NEUSTART»** Klicken.

***Dashboard erstellen:***

Gehe dafür auf die Schaltfläche **«Übersicht»**. du landest nun auf der **«Home»** Seite. Nun kannst du mittels den _(drei vertikalen Punkten)_ ein neues Dashboard erstellen. Bewege dafür den Schalter **«Beginne mit einem leeren Dashboard»** nach rechts und anschliessend **«KONTROLLE ÜBERNEHMEN»**.

Nun befindest du dich im Bearbeitungsmodus. Klicke nun als erstes auf den Stift rechts neben **«HOME»**. Im neuen Fenster wählst du unter **«Ansichtsart»** **«Panel (1 Karte)»**

Nun Klickst du auf die Schaltfläche **«+ KARTE HINZUFÜGEN»** und wählst **«Picture Element»** aus.
Füge nun die folgenden Code-Zeilen ein.
```ruby
type: picture-elements
elements:
  - type: state-label
    entity: sensor.x_acceleration
    style:
      top: 78%
      left: 16%
      font-size: 15px
  - type: state-label
    entity: sensor.y_acceleration
    style:
      top: 78%
      left: 21%
      font-size: 15px
  - type: state-label
    entity: sensor.z_acceleration
    tap_action:
      action: navigate
      navigation_path: /lovelace/beschleunigung
    style:
      top: 74%
      left: 16%
      font-size: 15px
  - type: state-label
    entity: sensor.temperatur
    style:
      top: 64%
      left: 28%
      font-size: 16px
  - type: state-label
    entity: sensor.humidity
    style:
      top: 63%
      left: 72%
      font-size: 16px
  - type: state-label
    entity: sensor.compass
    style:
      top: 73%
      left: 81%
      font-size: 22px
  - type: image
    image: http://192.168.0.222:9091/
    style:
      top: 33%
      left: 50%
      width: 36%
  - type: image
    image: https://iili.io/HnbY3hB.png
    style:
      top: 50%
      left: 50%
      width: 100%
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/271/271228.png
    style:
      top: 64%
      left: 50%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/bewegung
        payload: left
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/271/271228.png
    style:
      top: 60%
      left: 41%
      width: 2%
      rotate: 180deg
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/bewegung
        payload: right
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/271/271228.png
    style:
      top: 68%
      left: 56%
      width: 2%
      rotate: 90deg
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/bewegung
        payload: down
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/271/271228.png
    style:
      top: 55%
      left: 56%
      width: 2%
      rotate: 270deg
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/bewegung
        payload: up
      target: {}
  - type: image
    image: >-
      https://cdn.icon-icons.com/icons2/2699/PNG/512/grafana_logo_icon_171049.png
    tap_action:
      action: navigate
      navigation_path: /lovelace/beschleunigung
    name: Presets aanpassen
    style:
      top: 85%
      left: 14%
      width: 7%
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128883.png
    style:
      top: 40.2%
      left: 77%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 10
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128881.png
    style:
      top: 35.9%
      left: 77.6%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 11
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128881.png
    style:
      top: 35.9%
      left: 78.2%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 11
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128881.png
    style:
      top: 34%
      left: 80.5%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 12
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128886.png
    style:
      top: 34%
      left: 81.2%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 12
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128881.png
    style:
      top: 35%
      left: 84%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 1
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128886.png
    style:
      top: 39%
      left: 86%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 2
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128889.png
    style:
      top: 44%
      left: 86.5%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 3
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128892.png
    style:
      top: 48.3%
      left: 86%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 4
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128895.png
    style:
      top: 52%
      left: 84%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 5
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128898.png
    style:
      top: 52.7%
      left: 81.5%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 6
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128901.png
    style:
      top: 51.2%
      left: 79.5%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 7
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128904.png
    style:
      top: 48%
      left: 78%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 8
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/9128/9128907.png
    style:
      top: 45%
      left: 77.2%
      width: 2%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/display
        payload: 9
      target: {}
  - type: image
    image: https://cdn-icons-png.flaticon.com/512/5514/5514224.png
    style:
      top: 57%
      left: 87.7%
      width: 2.3%
    tap_action:
      action: call-service
      service: mqtt.publish
      data:
        topic: tank/aktor/schuss
        payload: s
      target: {}
image: https://iili.io/HofsIwb.jpg
```
Das Dashboard sollte nun wie folgt aussehen

![ALT](/Bilder/HomeAssistantDashboard.png)
 
## Grafana mit Home Assistant verknüpfen
#### _Grafana_
Als erstes gest du auf den Webserver von Grafana und gest auf dein Dashboard. Anschliessend Klickst du oben an deinem Diagram **«Panzer Beschleunigung»** und im Drop-Down Menü auf **«Share»**.

Im neune Fenster wählt du nun die Kachel **«Embed»** und Kopiert dir den Embed-Code **«Embed HTML»** in ein Texteditor. Nun müssen einige Anpassungen gemacht werden.

***erstens:*** Extrahiere den **Fett** markierten Teil aus dem Embed-Code ohne den Anführungszeichen

>iframe src="**http://192.168.0.222:3000/d-solo/9IYQoXFVk/tank?orgId=1&refresh=1s&from=1670779218875&to=1670779228875&panelId=2**" width="450" height="200" frameborder="0"></iframe

***zweitens:*** Überschreibe den **Fett** markierten Teil mit dem untenstehenden Code

>http://192.168.0.222:3000/d-solo/9IYQoXFVk/tank?orgId=1& **refresh=1s&from=1670779218875&to=1670779228875** &panelId=2

from=now-1m&to=now&refresh=1s

Anschliessend sollte der Embed-code wie folgt aussehen:

>http://192.168.0.222:3000/d-solo/9IYQoXFVk/tank?orgId=1&from=now-1m&to=now&refresh=1s&panelId=2

Speichere den Code für den nächsten Schritt ab.
	
#### _Home Assistant_
Öffne nun Home Assistant und gehe zu deinem Dashboard. Füge ein neues Daschboard Hinzu in dem du auf die (drei vertikalen Punkte) drückst und **«Benutzerfläche konfigurieren»** wählst. Danach kannst du oberhalb auf das «+» Zeichen Klicken und ein neues Fenster öffnet sich.

Gib al Titel ein **«Beschleunigung»** und wähle unter Ansichtsart **«Panel (1 Karte)»**.

Nun kannst du eine neue Karte hinzufügen mit **«+ KARTE HINZUFÜGEN»** und klicke auf Webseite.
Ein neues Fenster **«Website-Kartenkonfiguration»** öffnet sich. Nun kannst du unter dem Punkt **«URL*»** den Embed-Code einfügen und die Karte speichern.

Du sollstes jetzt das Diagramm aus Grafana auf deinem Monitor sehen und es sollte sich jede Sekunde updaten.

***Linke Seitenleiste von Home Assistant ausblenden:***

Klicke auf der linken Seitenleiste auf das unterste Icon **«Tank»** anschliessend suchs du die Einstellung **«Verstecke die Seitenleiste immer»** und Toggels den Slider auf True. Wenn du nun mit der **«F11»** Taste auf deiner Tastatur in den Vollbildmodus wechselst, ist keine Seitenleiste mehr zu sehen.

## Mindstorms NXT
### Mindstorms NXT Installation
Installiere NXT-Python.
```
python3 -m pip install --upgrade nxt-python
```
Installiere den USB-Treiber für Python falls dieser noch nicht installiert ist.
```
python -m pip install pyusb
```
Gehe ins Verzeichnis _«/etc/udev/rules.d»_ und füge eine Datei Namens **«60-libnxt.rules»** hinzu mit folgendem Inhalt.
```
sudo nano /etc/udev/rules.d/60-libnxt.rules
```
```ruby
# Allow access to the NXT brick.
SUBSYSTEM=="usb", ACTION=="add", ATTR{idVendor}=="0694", ATTR{idProduct}=="0002", \
    MODE="0660", GROUP="plugdev", TAG+="uaccess"
```
Übernehme die Änderungen der «udev» Schnittstelle mit folgenden Befehlen.
```
sudo udevadm control --reload
```
```
sudo udevadm trigger
```
```
sudo reboot
```
Nach dem Neustart kann die Verbindung zum NXT mit folgendem Befehl geprüft werden. Verbinde dafür den NXT mittels USB-Kabel mit dem Raspberry Pi und starte den NXT mit dem orangen Knopf. Nach dem Aufstarten sollten 5 Sekunden gewartet werden. Anschliessend kann der untenstehende Test durchgeführt werden. Der NXT gibt bei erfolgreichem Test, Töne von sich.
```
nxt_test
```
Falls es dabei Probleme geben sollte, kann der Befehl mit der Aktivierung des Debuggings erweitert werden.
```
nxt_test --log-level=debug
```

### Mindstorms NXT Ansteuerung
Erstelle ein neues Verzeichnis mit folgendem Befehl.
```
mkdir ~/nxt
```
Gehe nun in dieses Verzeichnis und erstelle eine neue Datei mit folgendem Inhalt.
```
nano ~/nxt/nxtPySc.py
```
```ruby
#!/usr/bin/env python
#

import nxt
import nxt.locator
from nxt.sensor import *
from nxt.motor import *
import paho.mqtt.subscribe as subscribe

def mqtt():
    msg = subscribe.simple("tank/aktor/bewegung", hostname="localhost")
    return str(msg.payload.decode("utf-8"))

ch = ' '

with nxt.locator.find() as b:
    left = b.get_motor(nxt.motor.Port.A)
    right = b.get_motor(nxt.motor.Port.B)
    both = nxt.motor.SynchronizedMotors(left, right, 0)
    leftboth = nxt.motor.SynchronizedMotors(left, right, 100)
    rightboth = nxt.motor.SynchronizedMotors(right, left, 100)

    while ch != 'quit':
        ch = mqtt()
        print(ch)
        if ch == 'up':
            both.turn(80, 360, True)
        elif ch == 'down':
            both.turn(-80, 360, True)
        elif ch == 'left':
            leftboth.turn(80, 90, True)
        elif ch == 'right':
            rightboth.turn(80, 90, True)
        elif ch == 'shoot':
            both.turn(80, 10, True)
            b.play_tone(50,100)
```
Nach dem Speichern der Datei musst du noch eine neue Python library installieren. Dafür musst du folgenden Befehl eingeben.
```
sudo pip install paho-mqtt
```

#### _Node-Red_
Nach der Installation gehst du in den Webserver von Node-Red. Hier importierst du eine weitere Datei (Flow). Dafür ist eine Datei im Repository _«TinL1_tank/Node_Red/NXT_Ansteuerung.json»_ abgelegt. Zugreifen auf das Repository kannst du über folgende URL
https://gitlab.fhnw.ch/timo.fischer/tinl1_tank.git

Die Datei kann mittels Drag&Drop oder über die Funktion **«Import»** im Webbrowser hochgeladen werden. Danach wird der Knopf **«Übernahme (deploy)»** betätigt. Sofern der NXT per USB-Kabel angeschlossen und eingeschalten ist, sollten keine Fehler auftreten. Falls doch, solltes du den Dateipfad im Block **«pythonshell in»** kontrollieren.

 
### Abschluss-Rohr auf dem Sense-Hat anzeigen
Erstelle ein neues Verzeichnis mit folgendem Befehl.
```
mkdir ~/shoot
```
Gehe nun in dieses Verzeichnis und erstelle eine neue Datei mit folgendem Inhalt.
```
nano ~/shoot/displaySensehat.py
```
```ruby
from sense_hat import SenseHat
import sys

sense = SenseHat()

R = [255, 0, 0]  # Red
W = [255, 255, 255]  # White
B = [0, 0, 0]  # Blank

X1 = B
X2 = B
X3 = B
X4 = B
X5 = B
X6 = B
X7 = B
X8 = B
X9 = B
X10 = B
X11 = B
X12 = B
Y1 = B
Y2 = B
Y3 = B
Y4 = B
Y5 = B
Y6 = B
Y7 = B
Y8 = B
Y9 = B
Y10 = B
Y11 = B
Y12 = B

O = B
X = W

input = sys.argv[1]

if input == '1' or input == '1s':
    X1 = W
    Y1 = W
    if 's' in input:
        Y1 = R
elif input == '2' or input == '2s':
    X2 = W
    Y2 = W
    if 's' in input:
        Y2 = R
elif input == '3' or input == '3s':
    X3 = W
    Y3 = W
    if 's' in input:
        Y3 = R
elif input == '4' or input == '4s':
    X4 = W
    Y4 = W
    if 's' in input:
        Y4 = R
elif input == '5' or input == '5s':
    X5 = W
    Y5 = W
    if 's' in input:
        Y5 = R
elif input == '6' or input == '6s':
    X6 = W
    Y6 = W
    if 's' in input:
        Y6 = R
elif input == '7' or input == '7s':
    X7 = W
    Y7 = W
    if 's' in input:
        Y7 = R
elif input == '8' or input == '8s':
    X8 = W
    Y8 = W
    if 's' in input:
        Y8 = R
elif input == '9' or input == '9s':
    X9 = W
    Y9 = W
    if 's' in input:
        Y9 = R
elif input == '10' or input == '10s':
    X10 = W
    Y10 = W
    if 's' in input:
        Y10 = R
elif input == '11' or input == '11s':
    X11 = W
    Y11 = W
    if 's' in input:
        Y11 = R
elif input == '12' or input == '12s':
    X12 = W
    Y12 = W
    if 's' in input:
        Y12 = R


rotation = [
Y11,O ,O ,Y12,Y1,O,O,Y2,
O,X11,O,X12,X1,O,X2,O,
O,O,X11,X12,X1,X2,O,O,
Y10,X10,X10,X,X,X3,X3,Y3,
Y9,X9,X9,X,X,X4,X4,Y4,
O,O,X8,X7,X6,X5,O,O,
O,X8,O,X7,X6,O,X5,O,
Y8,O,O,Y7,Y6,O,O,Y5
]

sense.set_pixels(rotation)
```
#### _Node-Red_
Nach der Installation gehst du in den Webserver von Node-Red. Hier importierst du eine weitere Datei (Flow). Dafür ist eine Datei im Repository _«TinL1_tank/Node_Red/Abschuss_Rohr.json»_ abgelegt. Zugreifen auf das Repository kannst du über folgende URL
https://gitlab.fhnw.ch/timo.fischer/tinl1_tank.git

Die Datei kann mittels Drag&Drop oder über die Funktion **«Import»** im Webbrowser hochgeladen werden. Danach wird der Knopf **«Übernahme (deploy)»** betätigt. 

## Bedienung des Panzers
Öffne das Dashboard auf Home Assitant über [IP-Adresse-Raspberry Pi]:8123. Es sollte wie folgt aussehen. 
 
Falls das Kamerabild nicht angezeigt wird, musst du in den Einstellungen des **«Picture Elements»** auf der Zeile 43 die aktuelle IP-Adresse deines Raspberry Pi’s angeben. Der Port soll dabei **«9091»** bleiben. Dasselbe Problem kann beim Beschleunigungs-Dashboard entstehen. Füge dabei dieselbe IP-Adresse ein.

# Dynamische Systemanalayse

Im Folgenden Abschnitt werden die verschiedenen dynamischen Abläufe des Systems grafisch vereinfacht aufgezeigt.

## Ansteuerung des Motors
![ALT](/Bilder/Ansteuerung_Motor.jpg)
 
## Livedaten anzeigen auf dem Dashboard
![ALT](/Bilder/Livedaten_auf_Dashboard.jpg)
 
## LED-Matrix auf dem Sense-Hat
![ALT](/Bilder/LED_Matrix_Sensehat.jpg)
 

